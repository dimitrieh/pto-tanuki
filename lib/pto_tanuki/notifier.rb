# frozen_string_literal: true

module PtoTanuki
  class Notifier
    TEMPLATE = <<~NOTE.strip
      @%<username>s

      :robot: [PTO
      Tanuki](https://gitlab.com/yorickpeterse/pto-tanuki):

      I am out of office from %<start_date>s until %<end_date>s. @rayana @nudalova 
      are currently covering while I am away.

    NOTE


    def initialize(gitlab, schedule)
      @gitlab = gitlab
      @schedule = schedule
    end

    def notify(todo)
      method =
        if todo.target_type == 'MergeRequest'
          :create_merge_request_note
        elsif todo.target_type == 'Issue'
          :create_issue_note
        end

      return unless method

      @gitlab.public_send(
        method,
        todo.target.project_id,
        todo.target.iid,
        note_body(todo)
      )
    end

    def note_body(todo)
      TEMPLATE % {
        username: todo.author.username,
        start_date: @schedule.start_date.iso8601,
        end_date: @schedule.end_date.iso8601
      }
    end
  end
end
